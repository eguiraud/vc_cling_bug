FROM archlinux

COPY packages packages
RUN pacman -Syu --noconfirm --quiet $(cat packages) && git clone https://github.com/root-project/root.git root_src
