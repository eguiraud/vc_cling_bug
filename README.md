# Reproduce issue with cling and vc on Arch Linux

```bash
$ docker build -t vc_cling_bug .
$ docker run -it vc_cling_bug
# mkdir root_build && cd root_build
# cmake -DCMAKE_BUILD_TYPE=Debug [-DLLVM_BUILD_TYPE=Debug] -Dtesting=ON -Droottest=ON -Droofit=OFF -Dtmva=OFF ../root_src
# cmake --build . -- -j4
```
